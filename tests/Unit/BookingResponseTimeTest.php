<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Proexe\BookingApp\Utilities\ResponseTimeCalculator;

class BookingResponseTimeTest extends TestCase
{

	public function setUp(): void {
		$this->sut = new ResponseTimeCalculator();
	}

	public function test_timeInTheSameDay_belowOneMinuteDifference()
	{
		$booking = '2018-01-16 12:38:00.0';
		$response = '2018-01-16 12:38:30.0';
		$officeHours = [];
		
		$this->assertEquals(
			0,
			$this->sut->calculate(
				$booking,
				$response,
				$officeHours
			)
		);
	}

	public function test_timeInTheSameDay_oneMinuteDifference()
	{
		$booking = '2018-01-16 12:38:00.0';
		$response = '2018-01-16 12:39:00.0';
		$officeHours = [];
		
		$this->assertEquals(
			1,
			$this->sut->calculate(
				$booking,
				$response,
				$officeHours
			)
		);
	}

	public function test_timeInTheSameDay_oneHourDifference()
	{
		$booking = '2018-01-16 12:38:00.0';
		$response = '2018-01-16 13:38:00.0';
		$officeHours = [];
		
		$this->assertEquals(
			60,
			$this->sut->calculate(
				$booking,
				$response,
				$officeHours
			)
		);
	}

	public function test_timeInFollowingDays_bothInsideOfficeHours()
	{
		$booking = '2020-04-05 12:38:00.0';
		$response = '2020-04-06 13:38:00.0';
		$officeHours = [
			[
				'to' => '13:00',
				'from' => '8:00',
				'isClosed' => false
			],
			[
				'to' => '18:00',
				'from' => '12:00',
				'isClosed' => false
			]
		];
		
		$this->assertEquals(
			120,
			$this->sut->calculate(
				$booking,
				$response,
				$officeHours
			)
		);
	}

	public function test_timeInFollowingDays_bookingOutsideOfficeHours()
	{
		$booking = '2020-04-05 12:38:00.0';
		$response = '2020-04-06 13:38:00.0';
		$officeHours = [
			[
				'to' => '12:00',
				'from' => '8:00',
				'isClosed' => false
			],
			[
				'to' => '18:00',
				'from' => '12:00',
				'isClosed' => false
			]
		];
		
		$this->assertEquals(
			98,
			$this->sut->calculate(
				$booking,
				$response,
				$officeHours
			)
		);
	}

	public function test_timeInFollowingDays_bookingOnClosedDay()
	{
		$booking = '2020-04-05 12:38:00.0';
		$response = '2020-04-06 13:38:00.0';
		$officeHours = [
			[
				'isClosed' => true
			],
			[
				'to' => '18:00',
				'from' => '12:00',
				'isClosed' => false
			]
		];
		
		$this->assertEquals(
			98,
			$this->sut->calculate(
				$booking,
				$response,
				$officeHours
			)
		);
	}

	public function test_timeInFollowingDays_responseOutsideOfficeHours()
	{
		$booking = '2020-04-05 12:38:00.0';
		$response = '2020-04-06 13:38:00.0';
		$officeHours = [
			[
				'to' => '13:00',
				'from' => '8:00',
				'isClosed' => false
			],
			[
				'to' => '18:00',
				'from' => '14:00',
				'isClosed' => false
			]
		];
		
		$this->assertEquals(
			22,
			$this->sut->calculate(
				$booking,
				$response,
				$officeHours
			)
		);
	}

	public function test_timeInFollowingDays_responseOnClosedDay()
	{
		$booking = '2020-04-05 12:38:00.0';
		$response = '2020-04-06 13:38:00.0';
		$officeHours = [
			[
				'to' => '13:00',
				'from' => '8:00',
				'isClosed' => false
			],
			[
				'isClosed' => true
			]
		];
		
		$this->assertEquals(
			22,
			$this->sut->calculate(
				$booking,
				$response,
				$officeHours
			)
		);
	}

	public function test_timeInFollowingDays_bothOutsideOfficeHours()
	{
		$booking = '2020-04-05 12:38:00.0';
		$response = '2020-04-06 13:38:00.0';
		$officeHours = [
			[
				'to' => '12:00',
				'from' => '8:00',
				'isClosed' => false
			],
			[
				'to' => '18:00',
				'from' => '14:00',
				'isClosed' => false
			]
		];
		
		$this->assertEquals(
			0,
			$this->sut->calculate(
				$booking,
				$response,
				$officeHours
			)
		);
	}

	public function test_timeInFollowingDays_bothOnClosedDay()
	{
		$booking = '2020-04-05 12:38:00.0';
		$response = '2020-04-06 13:38:00.0';
		$officeHours = [
			[
				'isClosed' => true
			],
			[
				'isClosed' => true
			]
		];
		
		$this->assertEquals(
			0,
			$this->sut->calculate(
				$booking,
				$response,
				$officeHours
			)
		);
	}

	public function test_timeInSepartedDays_bothInsideOfficeHours()
	{
		$booking = '2020-04-05 12:38:00.0';
		$response = '2020-04-07 13:38:00.0';
		$officeHours = [
			[
				'to' => '13:00',
				'from' => '8:00',
				'isClosed' => false
			],
			[
				'to' => '9:00',
				'from' => '8:00',
				'isClosed' => false
			],
			[
				'to' => '14:00',
				'from' => '13:00',
				'isClosed' => false
			],
		];
		
		$this->assertEquals(
			120,
			$this->sut->calculate(
				$booking,
				$response,
				$officeHours
			)
		);
	}
}
