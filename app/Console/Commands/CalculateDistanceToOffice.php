<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Proexe\BookingApp\Offices\Models\OfficeModel;
use Proexe\BookingApp\Utilities\DistanceCalculator;

class CalculateDistanceToOffice extends Command {

	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'bookingApp:calculateDistanceToOffice';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Calculates distance to office';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$distanceCalculator = new DistanceCalculator();
		$offices = OfficeModel::all()->toArray();

		$lat                = $this->ask( 'Enter Lat:', '14.12232322' );
		$lng                = $this->ask( 'Enter Lng:', '8.12232322' );
		$unit = 'km';
		$distances = [];

		foreach ($offices  as $office ) {

			$distance = $distanceCalculator->calculate(
				[ $lat, $lng ],
				[ $office['lat'], $office['lng'] ],
				$unit
			);

			$this->line( 'Distance to ' . $office['name']. ': ' . $distance . ' '. $unit);
			$distances[$office['id']] = $distance;
		}

		$closestOfficeIds = $distanceCalculator->findClosestOffice( [ $lat, $lng ], $offices, $distances);

		$closestOffices = OfficeModel::whereIn('id', $closestOfficeIds)->get();

		if (count($closestOffices) == 1) {
			$this->line( 'Closest office: ' . $closestOffices[0]->name );
		} else {
			$this->line( 'Closest offices: '  );
			foreach($closestOffices as $closestOffice) {
				$this->line($closestOffice->name);
			}
		}

		
	}
}
