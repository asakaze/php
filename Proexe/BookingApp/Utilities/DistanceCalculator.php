<?php
/**
 * Date: 08/08/2018
 * Time: 16:20
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;


class DistanceCalculator {

	/**
	 * @param array  $from
	 * @param array  $to
	 * @param string $unit - m, km
	 *
	 * @return mixed
	 */
	public function calculate( $from, $to, $unit = 'm' ) {
		$radius = $this->getRadius($unit);
		return number_format(
			$this->haversineGreatCircleDistance(
				$from[0],
				$from[1],
				$to[0],
				$to[1],
				$radius
			),
			2,
			'.',
			''
		);
	}

	/** 
	 * @param array $from
	 * @param array $offices
	 * @param array $distances
	 *
	 * @return array ids of closest offices
	 */
	public function findClosestOffice( $from, $offices, $distances = []) {

		if (empty($distances)) {
			foreach ($offices  as $office ) {
				$distances[$office['id']] = $this->calculate(
					$from,
					[
						$office['lat'],
						$office['lng']
					]
				);
			}
		}

		$minDistance = min($distances);
		return array_keys($distances, $minDistance);
	}

	private function getRadius(string $unit): int {
		$earthRadiusInMeters = 6371000;
		$unitModifier = [
			'm' => 1,
			'km' => 1000
		];

		return intval($earthRadiusInMeters / $unitModifier[$unit]);
	}

	private function haversineGreatCircleDistance(
		float $latitudeFrom,
		float $longitudeFrom,
		float $latitudeTo,
		float $longitudeTo,
		int $earthRadius = 6371000
	): float {
        $latitudeFromInRadians = deg2rad($latitudeFrom);
        $longitudeFromInRadians = deg2rad($longitudeFrom);
        $latitudeToInRadians = deg2rad($latitudeTo);
        $longitudeToInRadians = deg2rad($longitudeTo);

        $latDelta = $latitudeToInRadians - $latitudeFromInRadians;
        $lonDelta = $longitudeToInRadians - $longitudeFromInRadians;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latitudeFromInRadians) * cos($latitudeToInRadians) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }

}