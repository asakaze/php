<?php
/**
 * Date: 09/08/2018
 * Time: 00:16
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;

use Proexe\BookingApp\Offices\Interfaces\ResponseTimeCalculatorInterface;
use Carbon\Carbon;

class ResponseTimeCalculator {

	public function calculate($bookingDateTime, $responseDateTime, $officeHours) {
		$bookingDate = Carbon::createFromTimeString($bookingDateTime);
		$responseDate = Carbon::createFromTimeString($responseDateTime);

		if ($bookingDate->isSameDay($responseDate)) {
			return $bookingDate->diffInMinutes($responseDate);
		} else {
			$minutesToEndOfDay = $this->getTimeToEndOfDay($bookingDate, $officeHours);
			$minutesFromStartOfDay = $this->getTimeFromStartOfDay($responseDate, $officeHours);

			if (($responseDate->weekday() - $bookingDate->weekday()) != 1) {
				$skippedTime = $this->calculateTimeOnSkippedDays($bookingDate, $responseDate, $officeHours);
			} else {
				$skippedTime = 0;
			}

			return ($minutesToEndOfDay + $minutesFromStartOfDay + $skippedTime);
		}

		return 0;
	}

	private function getTimeToEndOfDay(Carbon $date, array $officeHours): int {
		if ($this->getOfficeHours($date, $officeHours)['isClosed']) {
			return 0;
		} // booking on closed office day

		$closingTime = $this->getHourAndMinutesFromString(
			$this->getClosingTime($date, $officeHours)
		);
		$endOfDay = Carbon::create(
			$date->year,
			$date->month,
			$date->day,
			$closingTime[0],
			$closingTime[1]
		);

		if ($date->gt($endOfDay)) {
			return 0;
		} // booking after office hours

		return $date->diffInMinutes($endOfDay);
	}

	private function getTimeFromStartOfDay(Carbon $date, array $officeHours): int {
		if ($this->getOfficeHours($date, $officeHours)['isClosed']) {
			return 0;
		} // booking was updated while office was closed, undefined condition - assuming 0

		$openingTime = $this->getHourAndMinutesFromString(
			$this->getOpeningTime($date, $officeHours)
		);
		$startOfDay = Carbon::create(
			$date->year,
			$date->month,
			$date->day,
			$openingTime[0],
			$openingTime[1]
		);

		if ($startOfDay->gt($date)) {
			return 0;
		} // booking was updated while office was closed, undefined condition - assuming 0

		return $startOfDay->diffInMinutes($date);
	}

	private function calculateTimeOnSkippedDays(Carbon $startDate, Carbon $endDate, array $officeHours): int {
		//not implemented
		return 0;
	}

	private function getOfficeHours(Carbon $date, array $officeHours): array {
		return $officeHours[$date->weekday()];
	}

	private function getClosingTime(Carbon $date, array $officeHours): string {
		return $this->getOfficeHours($date, $officeHours)['to'];
	}

	private function getOpeningTime(Carbon $date, array $officeHours): string {
		return $this->getOfficeHours($date, $officeHours)['from'];
	}

	private function getHourAndMinutesFromString(string $time): array {
		return explode(':', $time);
	}

}